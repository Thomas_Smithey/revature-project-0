import { Account } from "../entities";

/**
 * This is the service layer. An interface is made for
 * account services. This is where the services functions
 * are declared. Note that these functions have similar
 * naming conventions to those of the AccountDAO. This
 * is because we will be accessing the DAO through
 * service layer functions.
 */

export default interface AccountService
{
    registerAccount(account:Account):Promise<Account>;
    retrieveAllAccounts():Promise<Account[]>;
    retrieveAccountById(id:number):Promise<Account>;
    modifyAccount(account:Account):Promise<Account>;
    removeAccountById(id:number):Promise<boolean>;
}