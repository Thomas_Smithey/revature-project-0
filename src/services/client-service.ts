import { Client } from "../entities";

/**
 * This is the service layer. An interface is made for
 * client services. This is where the services functions
 * are declared. Note that these functions have similar
 * naming conventions to those of the ClientDAO. This
 * is because we will be accessing the DAO through
 * service layer functions.
 */

export default interface ClientService
{
    registerClient(client:Client):Promise<Client>;
    retrieveAllClients():Promise<Client[]>;
    retrieveClientById(id:number):Promise<Client>;
    modifyClient(client:Client):Promise<Client>;
    removeClientById(id:number):Promise<boolean>;
}