import { AccountDAO } from "../daos/account-dao";
import { AccountDaoPostgres } from "../daos/account-dao-postgres-impl";
import { Account } from "../entities";
import AccountService from "./account-service";

/**
 * This is the implementation of AccountService. This is
 * the service layer where we could add in business logic.
 * This project only requires that we return the function
 * calls of the DAO.
 */

export class AccountServiceImpl implements AccountService
{
    accountDao:AccountDAO = new AccountDaoPostgres();

    registerAccount(account:Account):Promise<Account>
    {
        return this.accountDao.createAccount(account);
    }

    retrieveAllAccounts():Promise<Account[]>
    {
        return this.accountDao.getAllAccounts();
    }

    retrieveAccountById(id:number):Promise<Account>
    {
        return this.accountDao.getAccountById(id);
    }

    modifyAccount(account:Account):Promise<Account>
    {
        return this.accountDao.updateAccount(account);
    }

    removeAccountById(id:number):Promise<boolean>
    {
        return this.accountDao.deleteAccountById(id);
    }
}