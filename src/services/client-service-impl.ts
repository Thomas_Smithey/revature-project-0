import { ClientDAO } from "../daos/client-dao";
import { ClientDaoPostgres } from "../daos/client-dao-postgres-impl";
import { Client } from "../entities";
import ClientService from "./client-service";

/**
 * This is the implementation of ClientService. This is
 * the service layer where we could add in business logic.
 * This project only requires that we return the function
 * calls of the DAO.
 */

export class ClientServiceImpl implements ClientService
{
    clientDao:ClientDAO = new ClientDaoPostgres();

    registerClient(client:Client):Promise<Client> // CREATE, POST
    {
        return this.clientDao.createClient(client);
    }

    retrieveAllClients():Promise<Client[]> // READ, GET
    {
        return this.clientDao.getAllClients();
    }

    retrieveClientById(id:number):Promise<Client> // READ, GET
    {
        return this.clientDao.getClientById(id);
    }

    modifyClient(client:Client):Promise<Client> // UPDATE, PUT
    {
        return this.clientDao.updateClient(client);
    }

    removeClientById(id:number):Promise<boolean> // DELETE
    {
        return this.clientDao.deleteClientById(id);
    }
}