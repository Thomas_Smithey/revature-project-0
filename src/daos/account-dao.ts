import { Account } from "../entities";

/**
 * This is the Account Data Access Object. Here, the interface is
 * declared with all of the necessary functions to perform CRUD 
 * operations on the tables of the database. These functions will 
 * return promises that contain account objects.
 */

export interface AccountDAO
{
    createAccount(account:Account):Promise<Account>; // CREATE
    getAllAccounts():Promise<Account[]>; // READ
    getAccountById(id:number):Promise<Account>; // READ
    updateAccount(account:Account):Promise<Account>; // UPDATE
    deleteAccountById(id:number):Promise<boolean>; // DELETE
}