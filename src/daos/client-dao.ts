import { Client } from "../entities";

/**
 * This is the Client Data Access Object. Here, the interface is
 * declared with all of the necessary functions to perform CRUD 
 * operations on the tables of the database. These functions will 
 * return promises that contain client objects.
 */

export interface ClientDAO
{
    createClient(client:Client):Promise<Client>; // CREATE
    getAllClients():Promise<Client[]>; // READ
    getClientById(id:number):Promise<Client>; // READ
    updateClient(client:Client):Promise<Client>; // UPDATE
    deleteClientById(id:number):Promise<boolean>; // DELETE
}