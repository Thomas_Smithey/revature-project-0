import { Client } from "../entities";
import { ClientDAO } from "./client-dao";
import { conn } from "../connection";
import { MissingResourceError } from "../errors";

/**
 * This is the implementation of the ClientDAO interface.
 * The functions that were declared in the interface are
 * now defined.
 */

export class ClientDaoPostgres implements ClientDAO
{
    // Binds the items in the values array with the sql statement.
    async createClient(client:Client):Promise<Client>
    {
        const sql:string = "insert into client (first_name) values ($1) returning id";
        const values = [client.first_name];
        const result = await conn.query(sql, values);
        client.id = result.rows[0].id;
        return client;
    }

    // Selects every record in the table client.
    async getAllClients():Promise<Client[]>
    {
        const sql:string = "select * from client";
        const result = await conn.query(sql);
        const clients:Client[] = [];

        for (const row of result.rows)
        {
            const client:Client = new Client(row.id, row.first_name);
            clients.push(client);
        }

        return clients;
    }

    // Selects the record from the table that matches the client's id passed in as an argument.
    async getClientById(id:number):Promise<Client>
    {
        const sql:string = "select * from client where id = $1";
        const values = [id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The client with id ${id} does not exist.`);
        }

        const row = result.rows[0];
        const client:Client = new Client(row.id, row.first_name);
        return client;
    }

    // Updates the first_name property of a client with the given id.
    async updateClient(client:Client): Promise<Client>
    {
        const sql:string = "update client set first_name = $1 where id = $2";
        const values = [client.first_name, client.id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The client with id ${client.id} does not exist.`);
        }

        return client;
    }

    // Deletes a client from the table client. The id passed in determines which client is deleted.
    async deleteClientById(id:number):Promise<boolean>
    {
        const sql:string = "delete from client where id = $1";
        const values = [id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The client with id ${id} does not exist.`);
        }

        return true;
    }
}