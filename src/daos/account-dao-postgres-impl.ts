import { Account } from "../entities";
import { AccountDAO } from "./account-dao";
import { conn } from "../connection";
import { MissingResourceError } from "../errors";

/**
 * This is the implementation of the AccountDAO interface.
 * The functions that were declared in the interface are
 * now defined.
 */

export class AccountDaoPostgres implements AccountDAO
{
    // Binds the items in the values array with the sql statement.
    async createAccount(account:Account):Promise<Account>
    {
        const sql:string = "insert into account (client_id, amount) values ($1, $2) returning id";
        const values = [account.client_id, account.amount];
        const result = await conn.query(sql, values);
        account.id = result.rows[0].id;
        return account;
    }

    // Selects every record in the table account.
    async getAllAccounts():Promise<Account[]>
    {
        const sql:string = "select * from account";
        const result = await conn.query(sql);
        const accounts:Account[] = [];

        for (const row of result.rows)
        {
            const account:Account = new Account
            (
                row.id,
                row.client_id,
                row.amount
            );
            accounts.push(account);
        }

        return accounts;
    }

    // Selects the record from the table that matches the account's id passed in as an argument.
    async getAccountById(id:number):Promise<Account>
    {
        const sql:string = "select * from account where id = $1";
        const values = [id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The account with id ${id} does not exist.`);
        }

        const row = result.rows[0];
        const account:Account = new Account
        (
            row.id, 
            row.client_id,
            row.amount
        );

        return account;
    }

    // Updates the amount property of an account with the given id.
    async updateAccount(account:Account):Promise<Account>
    {
        const sql:string = "update account set amount = $1 where id = $2";
        const values = [account.amount, account.id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The client with id ${account.id} does not exist.`);
        }

        return account;
    }

    // Deletes an account from the table account. The id passed in determines which account is deleted.
    async deleteAccountById(id:number):Promise<boolean>
    {
        const sql:string = "delete from account where id = $1";
        const values = [id];
        const result = await conn.query(sql, values);

        if (result.rowCount === 0)
        {
            throw new MissingResourceError(`The account with id ${id} does not exist.`);
        }

        return true;
    }
}