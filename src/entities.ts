/**
 * Create the two entities Client and Account.
 * These will serve as the most basic units from which
 * the rest of the project may be built with. These
 * entities need to have constructors with parameters
 * that match with the values in the database.
 * 
 * From the tables in the database:
 * 
 *     client: id int, first_name varchar(100)
 * 
 *     account: id int, client_id int, amount int
 * 
 * Note:
 *     - Both id properties are primary keys.
 *     - client_id in account is a foreign key that
 *       references the 'id' property in client.
 */

export class Client
{
    constructor(public id:number, public first_name:string){};
}

export class Account
{
    constructor(public id:number, public client_id:number, public amount:number ){};
}