import express from "express";
import ClientService from "./services/client-service";
import { ClientServiceImpl } from "./services/client-service-impl";
import { Client } from "./entities";
import AccountService from "./services/account-service";
import { AccountServiceImpl } from "./services/account-service-impl";
import { Account } from "./entities";
import { MissingResourceError } from "./errors";

const app = express();
app.use(express.json());

const clientService:ClientService = new ClientServiceImpl();
const accountService:AccountService = new AccountServiceImpl();

/**
 * A function that can check if a string contains any the following:
 * 1.) English characters
 * 2.) Numbers
 * 3.) Special characters
 * 4.) Spaces
 * 5.) Is of length 0 (is empty)
 * 
 * Can tell the function what to look for, specifically.
 * Will return true if any of the arguments passed in result to true.
 * I made this function for the purpose of avoiding large if statements.
 * 
 * Example:
 * validateData('Adam1', 0, 1, 0, 0, 0); // results in true because a number is found in the string.
 */
 function validateData(value:string, hasChars:number, hasNums:number, hasSpecial:number, hasSpaces:number, isEmpty:number):Boolean
 {
    const boolArr:boolean[] = [];
    
    // Checks if a string contains any characters.
    if (hasChars === 1)
    {
        boolArr.push(/[a-zA-Z]/g.test(value));
    }
    
    // Checks if a string contains any numbers.
    if (hasNums === 1)
    {
        boolArr.push(/\d/g.test(value));
    }

    // Checks if a string contains special characters.
    if (hasSpecial === 1)
    {
        boolArr.push(/[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(value));
    }

    // Checks if a string contains any spaces.
    if (hasSpaces === 1)
    {
        boolArr.push(/\s/.test(value));
    }

    // Checks if the length of the string is zero.
    if (isEmpty === 1)
    {
        boolArr.push(value.length === 0);
    }

    // Loops over the boolArr. If any value is true, then return true.
    for (const bool of boolArr)
    {
        if (bool)
        {
            return true;
        }
    }

    return false;
 }

/**
 * Adam's message from Gitlab:
 * POST /clients => Creates a new client
 * return a 201 status code
 */
app.post("/clients", async (req, res) =>
{
    if (Object.keys(req.body).length > 1 || !(req.body.hasOwnProperty("first_name")))
    {
        res.status(403);
        res.send({"message": "Error: cannot include properties other than first_name."});
        return;
    }

    if (validateData(req.body["first_name"], 0, 1, 1, 1, 1))
    {
        res.status(403);
        res.send({"message": "Error: first_name cannot contain numbers, special characters, or be empty. Must contain one name only."});
        return;
    }

    const client = await clientService.registerClient(req.body);
    res.status(201);
    res.send(client);
    return;
});

/**
 * Adam's message from Gitlab:
 * POST /clients/5/accounts =>creates a new account for client with the id of 5
 * return a 201 status code
 */
app.post("/clients/:id/accounts", async (req, res) =>
{
    const testClients = await clientService.retrieveAllClients();

    if (testClients.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table client is empty."});
        return;
    }

    if (Number(req.params.id).toString() === "NaN")
    {
        res.status(403);
        res.send({"message": `Error: invalid id (${req.params.id}) was found in the url.`});
        return;
    }

    try
    {
        const client:Client = await clientService.retrieveClientById(Number(req.params.id));
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }

    if (Object.keys(req.body).length > 1 || !(req.body.hasOwnProperty("amount")))
    {
        res.status(403);
        res.send({"message": "Error: cannot include properties other than amount."});
        return;
    }

    if (validateData(req.body["amount"], 1, 0, 1, 1, 1))
    {
        res.status(403);
        res.send({"message": "Error: amount cannot contain non-numeric characters, negative or empty values."});
        return;
    }
    
    let account:Account = req.body;
    account.client_id = Number(req.params.id);
    account = await accountService.registerAccount(account);
    res.status(201);
    res.send(account);
    return;
});

// This is the catch-all for post requests. Can only reached if there is no id entered in the url.
app.post("*", (req, res) =>
{
    res.status(403);
    res.send({"message": "Error: no id was found in the url."});
    return;
});

/**
 * Adam's message from Gitlab:
 * GET /clients => gets all clients return 200
 */
app.get("/clients", async (req, res) =>
{
    const clients:Client[] = await clientService.retrieveAllClients();

    if (clients.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table client is empty."});
        return;
    }
    
    res.status(200);
    res.send(clients);
    return;
});

/**
 * Adam's message from Gitlab:
 * GET /clients/10 => get client with id of 10
 * return 404 if no such client exist
 */
app.get("/clients/:id", async (req, res) =>
{
    const testClients = await clientService.retrieveAllClients();

    if (testClients.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table 'client' is empty."});
        return;
    }

    if (Number(req.params.id).toString() === "NaN")
    {
        res.status(403);
        res.send({"message": `Error: invalid id (${req.params.id}) was found in the url.`});
        return;
    }

    try
    {
        const client:Client = await clientService.retrieveClientById(Number(req.params.id));
        res.status(200);
        res.send(client);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

/**
 * Adam's message from Gitlab:
 * GET /clients/7/accounts => get all accounts for client 7
 * return 404 if no client exists
 */
app.get("/clients/:id/accounts", async (req, res) =>
{
    const testAccounts = await accountService.retrieveAllAccounts();

    if (testAccounts.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table account is empty."});
        return;
    }

    if (Number(req.params.id).toString() === "NaN")
    {
        res.status(403);
        res.send({"message": `Error: invalid id (${req.params.id}) was found in the url.`});
        return;
    }

    try
    {
        const client:Client = await clientService.retrieveClientById(Number(req.params.id));
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }

    const accounts:Account[] = await accountService.retrieveAllAccounts();
    let clientAccounts:Account[] = [];

    // Fill clientAccounts with all accounts with the client_id passed in the url.
    clientAccounts = accounts.filter((a) => 
    {
        if (a.client_id === Number(req.params.id))
        {
            return a;
        }
    });

    // If the client exists but doesn't have any accounts.
    if (clientAccounts.length === 0)
    {
        res.status(404);
        res.send({"message": `The client with id ${Number(req.params.id)} does not have any accounts.`});
        return;
    }
    
    // If the client exists and has one or more accounts.
    res.status(200);
    res.send(clientAccounts);
    return;
});

/**
 * Adam's message from Gitlab:
 * GET /accounts/4 => get account with id 4
 * return 404 if no account or client exists
 */
app.get("/accounts/:id", async (req, res) =>
{
    const testAccounts = await accountService.retrieveAllAccounts();

    if (testAccounts.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table account is empty."});
        return;
    }

    if (Number(req.params.id).toString() === "NaN")
    {
        res.status(403);
        res.send({"message": `Error: invalid id (${req.params.id}) was found in the url.`});
        return;
    }

    try
    {
        const account:Account = await accountService.retrieveAccountById(Number(req.params.id));
        res.status(200);
        res.send(account);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

/**
 * Adam's message from Gitlab:
 * (Optional)
 * GET /accounts?amountLessThan=2000&amountGreaterThan400 => get all accounts for between 400 and 200
 */
app.get("*", async (req, res) =>
{
    const accounts:Account[] = await accountService.retrieveAllAccounts();
    let clientAccounts:Account[] = [];
    const upperLimit:number = Number(req.query["amountLessThan"]);
    const lowerLimit:number = Number(req.query["amountGreaterThan"]);

    if (accounts.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table 'account' is empty."});
        return;
    }

    if (upperLimit.toString() === "NaN" && lowerLimit.toString() === "NaN")
    {
        res.status(403);
        res.send({"message": "Error: no valid id was found in the url or the limits have invalid values."});
        return;
    }

    if (upperLimit.toString() !== "NaN" && lowerLimit.toString() === "NaN")
    {
        clientAccounts = accounts.filter((a) => 
        {
            if (a.amount < upperLimit)
            {
                return a;
            }
        });
    }

    if (upperLimit.toString() === "NaN" && lowerLimit.toString() !== "NaN")
    {
        clientAccounts = accounts.filter((a) => 
        {
            if (a.amount > lowerLimit)
            {
                return a;
            }
        });
    }
    
    if (upperLimit.toString() !== "NaN" && lowerLimit.toString() !== "NaN")
    {
        if (lowerLimit > upperLimit)
        {
            res.status(403);
            res.send({"message": `Error: illegal query parameters set for amountGreaterThan(${lowerLimit}) and amountLessThan(${upperLimit}).`});
            return;
        }

        clientAccounts = accounts.filter((a) => 
        {
            if (a.amount < upperLimit && a.amount > lowerLimit)
            {
                return a;
            }
        });
    }

    // If there are no accounts in the given range of amount.
    if (clientAccounts.length === 0)
    {
        let errorMessage:object = {"message": "Error: no accounts found in the specified range of "};
        if (lowerLimit.toString() !== "NaN"){errorMessage["message"] += `${lowerLimit} < `;}
        errorMessage["message"] += "amount";
        if (upperLimit.toString() !== "NaN"){errorMessage["message"] += ` < ${upperLimit}`;}
        res.status(404);
        res.send(errorMessage);
        return;
    }
    
    // If there are accounts in the given range of amount.
    res.status(200);
    res.send(clientAccounts);
    return;
});

/**
 * Adam's message from Gitlab:
 * PUT /clients/12 => updates client with id of 12
 * return 404 if no such client exist
 */
app.put("/clients/:id", async (req, res) =>
{
    const testClients = await clientService.retrieveAllClients();

    if (testClients.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table 'client' is empty."});
        return;
    }

    if (Number(req.params.id).toString() === "NaN")
    {
        res.status(403);
        res.send({"message": `Error: invalid id (${req.params.id}) was found in the url.`});
        return;
    }

    if (Object.keys(req.body).length > 2 || !(req.body.hasOwnProperty("first_name") && req.body.hasOwnProperty("id")))
    {
        res.status(403);
        res.send({"message": "Error: cannot include properties other than id and first_name."});
        return;
    }

    if (validateData(req.body["first_name"], 0, 1, 1, 1, 1))
    {
        res.status(403);
        res.send({"message": "Error: first_name cannot contain numbers, special characters, spaces, or be empty."});
        return;
    }

    if (Number(req.params.id) !== req.body.id)
    {
        res.status(403);
        res.send({"message": `Error: The id in the url (${Number(req.params.id)}) does not match the id in the body (${req.body.id}).`});
        return;
    }

    try
    {
        const client:Client = await clientService.modifyClient(req.body);
        res.status(200);
        res.send(client);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

/**
 * Adam's message from Gitlab:
 * PUT /accounts/3 => update account with the id 3 
 * return 404 if no account or client exists
 */
app.put("/accounts/:id", async (req, res) =>
{
    const testAccounts = await accountService.retrieveAllAccounts();

    if (testAccounts.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table 'account' is empty."});
        return;
    }

    if (Number(req.params.id).toString() === "NaN")
    {
        res.status(403);
        res.send({"message": `Error: invalid id (${req.params.id}) was found in the url.`});
        return;
    }

    if (Object.keys(req.body).length > 3 || !(req.body.hasOwnProperty("id") && req.body.hasOwnProperty("client_id") && req.body.hasOwnProperty("amount")))
    {
        res.status(403);
        res.send({"message": "Error: cannot include properties other than id, client_id and amount."});
        return;
    }

    if (validateData(req.body["client_id"], 1, 0, 1, 1, 1) || validateData(req.body["amount"], 1, 0, 1, 1, 1))
    {
        res.status(403);
        res.send({"message": "Error: id, client_id and amount cannot contain non-numeric characters, negative or empty values."});
        return;
    }

    if (Number(req.params.id) !== req.body.id)
    {
        res.status(403);
        res.send({"message": `Error: The id in the url (${Number(req.params.id)}) does not match the id in the body (${req.body.id}).`});
        return;
    }

    try
    {
        const testAccount:Account = await accountService.retrieveAccountById(Number(req.params.id));

        if (testAccount.client_id !== req.body["client_id"])
        {
            res.status(403);
            res.send({"message": `Error: account with id ${testAccount.id} belongs to the client with id ${testAccount.client_id}. Cannot modify client_id.`});
            return;
        }

        const account = await accountService.modifyAccount(req.body);
        res.status(200);
        res.send(account);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

// This is the catch-all for put requests. Can only reached if there is no id entered in the url.
app.put("*", (req, res) =>
{
    res.status(403);
    res.send({"message": "Error: no id was found in the url."});
    return;
});

/**
 * Adam's message from Gitlab:
 * PATCH /accounts/12/deposit => deposit given amount (Body {"amount":500} ) 
 * return 404 if no account exists
 */
app.patch("/accounts/:id/deposit", async (req, res) =>
{
    const testAccounts = await accountService.retrieveAllAccounts();

    if (testAccounts.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table 'account' is empty."});
        return;
    }

    if (Number(req.params.id).toString() === "NaN")
    {
        res.status(403);
        res.send({"message": `Error: invalid id (${req.params.id}) was found in the url.`});
        return;
    }

    if (Object.keys(req.body).length > 1 || !(req.body.hasOwnProperty("amount")))
    {
        res.status(403);
        res.send({"message": "Error: cannot include properties other than amount."});
        return;
    }

    if (validateData(req.body["amount"], 1, 0, 1, 1, 1))
    {
        res.status(403);
        res.send({"message": "Error: amount cannot contain non-numeric characters, negative or empty values."});
        return;
    }

    if (Number(req.body["amount"]) === 0)
    {
        res.status(403);
        res.send({"message": "Error: deposit must be greater than zero."});
        return;
    }

    try
    {
        let account = await accountService.retrieveAccountById(Number(req.params.id));        
        account.amount += Number(req.body["amount"]);
        account = await accountService.modifyAccount(account);
        res.status(200);
        res.send(account);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

/**
 * Adam's message from Gitlab:
 * PATCH /accounts/12/withdraw => deposit given amount (Body {"amount":500} ) 
 * return 404 if no account exists 
 * return 422 if insufficient funds
 */
app.patch("/accounts/:id/withdraw", async (req, res) =>
{
    const testAccounts = await accountService.retrieveAllAccounts();

    if (testAccounts.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table 'account' is empty."});
        return;
    }

    if (Number(req.params.id).toString() === "NaN")
    {
        res.status(403);
        res.send({"message": `Error: invalid id (${req.params.id}) was found in the url.`});
        return;
    }

    if (Object.keys(req.body).length > 1 || !(req.body.hasOwnProperty("amount")))
    {
        res.status(403);
        res.send({"message": "Error: cannot include properties other than amount."});
        return;
    }

    if (validateData(req.body["amount"], 1, 0, 1, 1, 1))
    {
        res.status(403);
        res.send({"message": "Error: amount cannot contain non-numeric characters, negative or empty values."});
        return;
    }

    if (Number(req.body["amount"]) <= 0)
    {
        res.status(403);
        res.send({"message": `Error: ${req.body["amount"]} is an invalid value for a withdrawal.`});
        return;
    }

    try
    {
        let account = await accountService.retrieveAccountById(Number(req.params.id));

        if ((account.amount - Number(req.body["amount"])) < 0)
        {
            res.status(422);
            res.send({"message": `Error: cannot withdraw ${req.body["amount"]} from account with ${account.amount}.`});
            return;
        }

        account.amount -= Number(req.body["amount"]);
        account = await accountService.modifyAccount(account);
        res.status(200);
        res.send(account);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

// This is the catch-all for patch requests. Can only reached if there is no id entered in the url.
app.patch("*", (req, res) =>
{
    res.status(403);
    res.send({"message": "Error: no id was found in the url."});
    return;
});

/**
 * Adam's message from Gitlab:
 * DELETE /clients/15 => deletes client with the id of 15
 * return 404 if no such client exist
 * return 205 if success
 */
app.delete("/clients/:id", async (req, res) =>
{
    const testClients = await clientService.retrieveAllClients();

    if (testClients.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table 'client' is empty."});
        return;
    }

    if (Number(req.params.id).toString() === "NaN")
    {
        res.status(403);
        res.send({"message": `Error: invalid id (${req.params.id}) was found in the url.`});
        return;
    }

    try
    {
        const client = await clientService.removeClientById(Number(req.params.id));
        res.status(205);
        res.send(client);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

/**
 * Adam's message from Gitlab:
 * DELETE /accounts/6 => delete account 6
 * return 404 if no account or client exists
 */
app.delete("/accounts/:id", async (req, res) =>
{
    const testAccounts = await accountService.retrieveAllAccounts();

    if (testAccounts.length === 0)
    {
        res.status(404);
        res.send({"message": "Error: table 'account' is empty."});
        return;
    }

    if (Number(req.params.id).toString() === "NaN")
    {
        res.status(403);
        res.send({"message": `Error: invalid id (${req.params.id}) was found in the url.`});
        return;
    }

    try
    {
        const account = await accountService.removeAccountById(Number(req.params.id));
        res.status(205);
        res.send(account);
        return;
    }
    catch (error)
    {
        if (error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }
});

// This is the catch-all for delete requests. Can only reached if there is no id entered in the url.
app.delete("*", (req, res) =>
{
    res.status(403);
    res.send({"message": "Error: no id was found in the url."});
    return;
});

// listen for this application on port 3000.
app.listen(3000, () => {console.log("Application started")});