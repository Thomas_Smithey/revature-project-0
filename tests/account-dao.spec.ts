import { conn } from "../src/connection";
import { AccountDAO } from "../src/daos/account-dao";
import { AccountDaoPostgres } from "../src/daos/account-dao-postgres-impl";
import { Account } from "../src/entities";

/**
 * This is a series of jest tests that ensure CRUD
 * operations can be performed on the tables in our
 * database.
 * 
 * Both tables client and account must exist before
 * runing these tests for a successful outcome.
 * 
 * The first test ensures that the existing table
 * is empty, dropped and recreated. This allows for
 * testing CRUD operations in the remaining tests.
 */

const accountDao:AccountDAO = new AccountDaoPostgres();

/**
 * Delete all records in table account and drop it.
 * Then creates a new account table. This is a necessary
 * first step to ensure that the test can be executed
 * more than once.
 */
test("Clean up account table" , async () =>
{
    let sql:string = "delete from account";
    let result = await conn.query(sql);
    sql = "drop table account";
    result = await conn.query(sql);
    sql = "create table account (id int primary key generated always as identity, client_id int, amount int, constraint fk_account_client foreign key (client_id) references client (id) on delete cascade)";
    result = await conn.query(sql);
    sql = "select * from account";
    result = await conn.query(sql);
    expect(result.rowCount).toBe(0);
});

// Tests the createAccount function in the AccountDAO.
test("Create an account", async () =>
{
    const testAccount = new Account(1, 1, 200);
    const result = await accountDao.createAccount(testAccount);
    expect(result.id).not.toBe(0);
});

// Tests the getAllAccounts function in the AccountDAO.
test("Get all accounts", async () =>
{
    let account1:Account = new Account(1, 1, 100);
    let account2:Account = new Account(2, 1, 200);
    let account3:Account = new Account(3, 1, 300);

    await accountDao.createAccount(account1);
    await accountDao.createAccount(account2);
    await accountDao.createAccount(account3);

    const accounts:Account[] = await accountDao.getAllAccounts();
    expect(accounts.length).toBeGreaterThanOrEqual(3);
});

// Tests the getAccountById function in the AccountDAO.
test("Get account by Id", async () =>
{
    let account:Account = new Account(1, 1, 1);
    account = await accountDao.createAccount(account);
    let retrievedAccount:Account = await accountDao.getAccountById(account.id);
    expect(retrievedAccount.id).toBe(account.id);
});

// Tests the updateAccount function in the AccountDAO.
test("Update account", async () =>
{
    let account:Account = new Account(1, 1, 1);
    account = await accountDao.createAccount(account);
    account.amount = 100;
    account = await accountDao.updateAccount(account);
    expect(account.amount).toBe(100);
});

// Tests the deleteAccountById function in the AccountDAO.
test("Delete account by Id", async () =>
{
    let account:Account = new Account(1, 1, 1);
    account = await accountDao.createAccount(account);
    const result:boolean = await accountDao.deleteAccountById(account.id);
    expect(result).toBeTruthy();
});

// Closes the connection after the tests finish.
afterAll(async () =>
{
    conn.end();
});