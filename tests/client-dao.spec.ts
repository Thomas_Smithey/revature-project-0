import { conn } from "../src/connection";
import { ClientDAO } from "../src/daos/client-dao";
import { ClientDaoPostgres } from "../src/daos/client-dao-postgres-impl";
import { Client } from "../src/entities";

/**
 * This is a series of jest tests that ensure CRUD
 * operations can be performed on the tables in our
 * database.
 * 
 * Both tables client and account must exist before
 * runing these tests for a successful outcome.
 * 
 * The first three tests ensure that the existing tables
 * are empty, dropped and recreated. This allows for
 * testing CRUD operations in the remaining tests.
 */

const clientDAO:ClientDAO = new ClientDaoPostgres();

/**
 * Delete all records in the account table. Then drop the account 
 * table. This allows us to drop the client table. This is a necessary
 * first step because the account table depends on the client table.
 */
test("Delete and drop account table" , async () =>
{
    let sql:string = "delete from account";
    let result = await conn.query(sql);
    sql = "drop table account";
    result = await conn.query(sql);
    expect(result).toBeTruthy();
});

/**
 * Delete all records in the client table. Then drop the client
 * table. A new client table will be made for the purpose of testing.
 */
test("Clean up client table" , async () =>
{
    let sql:string = "delete from client";
    let result = await conn.query(sql);
    sql = "drop table client";
    result = await conn.query(sql);
    sql = "create table client(id int primary key generated always as identity, first_name varchar(100))";
    result = await conn.query(sql);
    sql = "select * from client";
    result = await conn.query(sql);
    expect(result.rowCount).toBe(0);
});

/**
 * Now that a new client table has been created, a new account
 * table needs to be created. This ensures that the test suite
 * can be executed more than once.
 */
test("Rebuild account table" , async () =>
{
    let sql:string = "create table account (id int primary key generated always as identity, client_id int, amount int, constraint fk_account_client foreign key (client_id) references client (id) on delete cascade)";
    let result = await conn.query(sql);
    sql = "select * from account";
    result = await conn.query(sql);
    expect(result.rowCount).toBe(0);
});

// Tests the createClient function in the ClientDAO.
test("Create a client", async () =>
{
    const testClient = new Client(0, "Bob");
    const result = await clientDAO.createClient(testClient);
    expect(result.id).not.toBe(0);
});

// Tests the getAllClients function in the ClientDAO.
test("Get all clients", async () =>
{
    let client1:Client = new Client(1, "Ben");
    let client2:Client = new Client(2, "Tom");
    let client3:Client = new Client(3, "Alan");

    await clientDAO.createClient(client1);
    await clientDAO.createClient(client2);
    await clientDAO.createClient(client3);

    const clients:Client[] = await clientDAO.getAllClients();
    expect(clients.length).toBeGreaterThanOrEqual(3);
});

// Tests the getClientById function in the ClientDAO.
test("Get client by Id", async () =>
{
    let client:Client = new Client(0, "Adam");
    client = await clientDAO.createClient(client);
    let retrievedClient:Client = await clientDAO.getClientById(client.id);
    expect(retrievedClient.id).toBe(client.id);
});

// Tests the updateClient function in thee ClientDAO.
test("Update client", async () =>
{
    let client:Client = new Client(0, "Bobby");
    client = await clientDAO.createClient(client);
    // to update an object we just edit it and then pass it to a method.
    client.first_name = "Richard";
    client = await clientDAO.updateClient(client);
    expect(client.first_name).toBe("Richard");
});

// Tests the deleteClientById function in the ClientDAO.
test("Delete client by Id", async () =>
{
    let client:Client = new Client(0, "Ray");
    client = await clientDAO.createClient(client);
    const result:boolean = await clientDAO.deleteClientById(client.id);
    expect(result).toBeTruthy();
});

// Closes the connection after the tests finish.
afterAll(async () =>
{
    conn.end();
});