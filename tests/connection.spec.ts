import { conn } from "../src/connection";

// Test the connection to the instance on GCP.
test("Should create a connection", async () =>
{
    const result = await conn.query("select * from client");
});

// Closes the connection after the test finishes.
afterAll(async () =>
{
    conn.end();
});